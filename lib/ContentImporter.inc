<?php
class ContentImporter {
  /**
   * Main info hook name.
   */
  const HOOK_NAME = 'content_importer_info';

  /**
   * Modules store files in this directory.
   */
  const DIR_NAME = 'content_importer';

  /**
   * Main variable storage for simple storage.
   */
  const VAR_NAME = 'content_importer_content';

  /**
   * Local static storage for all import info.
   *
   * @var array
   */
  protected $info;

  /**
   * Main function to get all import information.
   *
   * @return array
   *   Fully loaded array of import info by unique key.
   */
  public function info() {
    if (!isset($this->info)) {
      $modules = module_implements(self::HOOK_NAME);
      foreach ($modules as $module) {
        foreach (module_invoke($module, self::HOOK_NAME) as $id => $values) {
          $this->processFiles($module, $id, $values);
          $values['status'] = $this->get($id) ? 'imported' : 'pending';
          if (empty($values['uuid']) && $this->get($id)) {
            $values['pending_uuid'] = $this->get($id);
          }
          $this->info[$id] = $values;
        }
      }
    }
    return $this->info;
  }

  /**
   * Load a single import array by key.
   */
  public function load($key) {
    $info = $this->info();
    return !empty($info[$key]) ? $info[$key] : FALSE;
  }

  /**
   * Get an array of current pending imports.
   */
  public function pending() {
    $pending = array();
    foreach ($this->info() as $key => $info) {
      if ($info['status'] = 'pending') {
        $pending[$key] = $key;
      }
    }
    return $pending;
  }

  /**
   * Import a single piece of content.
   */
  public function import($key, $info = NULL) {
    $info = $info ? $info : (object) $this->load($key);
    if (!empty($info->uuid) && $this->getIdByUuid($info->entity_type, $info->uuid)) {
      drupal_set_message(t('UUID [!uuid] has already been created and will be skipped.', array(
        '!uuid' => $info->uuid,
      )), 'debug');
      return;
    }
    if ($this->get($key)) {
      drupal_set_message(t('Key [!key] has already been created and is identified by UUID [!uuid]. Please update your configuration.', array(
        '!key' => $key,
        '!uuid' => $this->get($key),
      )), 'debug');
      return;
    }
    $this->runImport($key, $info);
  }

  /**
   * Import all pending content.
   */
  public function importAll() {
    foreach ($this->info() as $key => $info) {
      $this->import($key, $info);
    }
  }

  /**
   * Add field info from external files.
   *
   * @param string $module
   *   Module name.
   * @param string $key
   *   Unique key for the import.
   * @param array $values
   *   Array of import info.
   */
  protected function processFiles($module, $key, &$values) {
    foreach ($this->searchFiles($module, $key) as $uri => $file) {
      $field = str_replace($key . '.', '', $file->name);
      $value = file_get_contents($uri);
      $values[$field] = $value;
    }
  }

  /**
   * Search a drupal module for external files.
   *
   * @param string $module
   *   Module name.
   * @param string $key
   *   Unique key for the import.
   *
   * @return array
   *   Array of file info keyed by uri.
   */
  protected function searchFiles($module, $key) {
    $dir = drupal_get_path('module', $module) . '/' . self::DIR_NAME;
    return file_scan_directory($dir, "/$key./");
  }


  /**
   * Run a content import.
   *
   * @param string $key
   *   Unique ID of the import.
   * @param array $info
   *   Array of import info.
   */
  protected function runImport($key, $info) {
    $info = (object) $info;
    $entity = entity_create($info->entity_type, array('type' => $info->bundle));
    $wrapper = entity_metadata_wrapper($info->entity_type, $entity);
    if (!empty($info->uuid)) {
      $this->setUuid($info->entity_type, $entity, $info->uuid);
      unset($info->uuid);
    }
    unset($info->bundle);
    unset($info->entity_type);
    unset($info->status);
    foreach ($info as $property => $value) {
      drupal_alter('content_importer_value', $entity, $property, $value);
      $wrapper->{$property}->set($value);
    }
    $wrapper->save();
    $uuid = $wrapper->uuid->value();
    $this->set($key, $uuid);
    drupal_set_message(t('Content [!key] created with UUID [!uuid]', array(
      '!key' => $key,
      '!uuid' => $uuid,
    )), 'debug');
  }

  /**
   * Run callback for hook_entity_delete().
   */
  public function hook_entity_delete($entity, $entity_type) {
    if (!$uuid = $this->getUuid($entity_type, $entity)) {
      return;
    }
    $content = variable_get(self::VAR_NAME, array());
    if ($key = array_search($uuid, $content)) {
      $this->del($key);
    }
  }

  /**
   * Delete a key/UUID from main storage.
   */
  protected function del($key) {
    $content = variable_get(self::VAR_NAME, array());
    unset($content[$key]);
    variable_set(self::VAR_NAME, $content);
  }

  /**
   * Get UUID from main storage.
   */
  protected function get($key) {
    $content = variable_get(self::VAR_NAME, array());
    return !empty($content[$key]) ? $content[$key] : FALSE;
  }

  /**
   * Set UUID to main storage.
   */
  protected function set($key, $uuid) {
    $content = variable_get(self::VAR_NAME, array());
    $content[$key] = $uuid;
    variable_set(self::VAR_NAME, $content);
  }

  /**
   * Get the UUID of an entity.
   */
  protected function getUuid($entity_type, $entity) {
    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    $property_info = $wrapper->getPropertyInfo();
    if (!empty($property_info['uuid'])) {
      return $wrapper->uuid->value();
    }
    return FALSE;
  }

  /**
   * Set UUID on an entity.
   *
   * This is not possible with the entity_metadata_wrapper().
   */
  protected function setUuid($entity_type, $entity, $uuid) {
    $info = entity_get_info($entity_type);
    if (isset($info['uuid']) && $info['uuid'] == TRUE && !empty($info['entity keys']['uuid'])) {
      $uuid_key = $info['entity keys']['uuid'];
      if (empty($entity->{$uuid_key})) {
        $entity->{$uuid_key} = $uuid;
      }
    }
  }

  /**
   * Get the entity id by a uuid.
   */
  protected function getIdByUuid($entity_type, $uuid) {
    return entity_get_id_by_uuid($entity_type, array($uuid));
  }
}
