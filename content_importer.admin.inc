<?php

/**
 * @file
 * Content Importer admin functionality.
 */

/**
 * Page callback for admin form.
 */
function content_importer_admin_form($form, &$form_state) {
  $content = content_importer();
  $form['imports'] = array(
    '#type' => 'checkboxes',
    '#pre_render' => array('content_importer_pre_render_checkboxes'),
    '#info' => $content->info(),
  );

  foreach ($content->info() as $key => $info) {
    $form['imports']['#options'][$key] = $key;
  }

  $form['import'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Custom pre_render function for content_importer_admin_form().
 *
 * Helper pre render function to make a table.
 */
function content_importer_pre_render_checkboxes($element) {
  $table = array(
    '#theme' => 'table',
    '#header' => array(
      '',
      t('Status'),
      t('Bundle'),
      t('Entity type'),
      t('UUID'),
    ),
  );
  if (module_exists('devel')) {
    $table['#header'][] = '';
  }
  foreach (element_children($element) as $id) {
    $info = $element['#info'][$id];
    $uuid = empty($info['uuid']) ? 'No UUID saved' : $info['uuid'];
    if (!empty($info['pending_uuid']) && empty($info['uuid'])) {
      $uuid = 'Pending: ' . $info['pending_uuid'];
    }
    $td = array(
      drupal_render($element[$id]),
      $info['status'],
      $info['bundle'],
      $info['entity_type'],
      $uuid,
    );
    if (module_exists('devel')) {
      $td[] = l('Debug', 'admin/content/importer/' . $id);
    }
    $table['#rows'][] = $td;
    unset($element[$id]);
  }
  $element['table'] = $table;
  return $element;
}

/**
 * Form submit callback for content_importer_admin_form().
 */
function content_importer_admin_form_submit($form, &$form_state) {
  $values = array_filter($form_state['values']['imports']);
  foreach ($values as $key) {
    content_importer()->import($key);
  }
}
