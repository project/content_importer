<?php
/**
 * @file
 * Hook information about Content Importer.
 */

/**
 * Creates a new imported entity.
 */
function hook_content_importer_info() {
  return array(
    'unique-key-identifier' => array(
      'title' => 'Content title',
      'field_custom_field' => 'custom field value set by the metadata wrapper',
      'entity_type' => 'node',
      'bundle' => 'page',
      'uuid' => 'auto-generated-uuid',
    ),
  );
}

/**
 * Allow modules to alter values from the info hook before they are saved.
 */
function hook_content_importer_value_alter($entity, $property_name, &$value) {
  if ($property_name == 'my_custom_field') {
    $value = array(
      'key' => $value,
    );
  }
}
